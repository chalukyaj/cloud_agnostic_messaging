/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cloud_agnostic_messaging

import (
	"fmt"
	"time"

	AWS "gitlab.com/chalukyaj/cloud_agnostic_messaging/AWS"
	Azure "gitlab.com/chalukyaj/cloud_agnostic_messaging/Azure"
	Base "gitlab.com/chalukyaj/cloud_agnostic_messaging/Base"
	GCP "gitlab.com/chalukyaj/cloud_agnostic_messaging/GCP"
)

type PubSub struct {
	b           Base.Base
	ACCOUNT_ID  string `json:"account_id"`
	REGION      string `json:"region"`
	PROJECT     string `json:"project"`
	GCP_PROJECT string `json:"gcp_project"`

	ManagerObj interface{}
	SQSManager interface{}
}

func (p *PubSub) Init(args map[string]interface{}) {
	p.b.Init(args)
	p.ACCOUNT_ID = p.b.GetParam(args, "account_id", "").(string)
	p.REGION = p.b.GetParam(args, "region", "us-east-1").(string)
	p.PROJECT = p.b.GetParam(args, "project", "").(string)
	p.GCP_PROJECT = p.b.GetParam(args, "gcp_project", "").(string)

	switch p.PROJECT {
	case "aws":
		p.ManagerObj = new(AWS.SNS)
		p.ManagerObj.(*AWS.SNS).Init(args)
		p.SQSManager = new(AWS.SQS)
		p.SQSManager.(*AWS.SQS).Init(args)
	case "gcp":
		p.ManagerObj = new(GCP.PubSub)
		p.ManagerObj.(*GCP.PubSub).Init(args)
	case "azure":
		p.ManagerObj = new(Azure.SBus)
		p.ManagerObj.(*Azure.SBus).Init(args)
	default:
		p.b.CausePanic(
			fmt.Sprintf(
				"Invalid `PROJECT` %s",
				p.PROJECT,
			),
			nil,
		)
	}
}

func (p *PubSub) Publish(args map[string]interface{}) error {
	switch conn := p.ManagerObj.(type) {
	case *AWS.SNS:
		args["topic_arn"] = p.b.GetAwsArn("sns", p.REGION, p.ACCOUNT_ID, p.b.GetParam(args, "topic_name").(string))
		return conn.Publish(args)
	case *GCP.PubSub:
		return conn.Publish(args)
	case *Azure.SBus:
		return conn.Publish(args)
	}
	return nil
}

func (p *PubSub) Subscribe(args map[string]interface{}, msgChannel chan map[string]interface{}) {
	args["callback"] = func(args map[string]interface{}) {
		msgChannel <- args
	}

	switch conn := p.ManagerObj.(type) {
	case *AWS.SNS:
		args["topic_arn"] = p.b.GetAwsArn("sns", p.REGION, p.ACCOUNT_ID, p.b.GetParam(args, "topic_name").(string))
		args["protocol"] = p.b.GetParam(args, "protocol", "sqs").(string)

		if args["protocol"] == "sqs" {
			args["endpoint"] = p.b.GetAwsArn("sqs", p.REGION, p.ACCOUNT_ID, p.b.GetParam(args, "queue_name").(string))
		}

		_, err := conn.Subscribe(args)
		if err != nil {
			p.b.CausePanic(
				"Error Creating Subscription (SNS)",
				err,
			)
		}

		if args["protocol"] == "sqs" {
			executor := func() {
				for {
					err := p.SQSManager.(*AWS.SQS).ReceiveMessage(args)
					if err != nil {
						p.b.CausePanic(
							"Error Receiving Message From (SQS)",
							err,
						)
					}
					time.Sleep(time.Second)
				}
			}
			go executor()
		}
		break
	case *GCP.PubSub:
		err := conn.Subscribe(args)
		if err != nil {
			p.b.CausePanic(
				"Unable to subscribe (GCP)",
				err,
			)
		}
		break
	case *Azure.SBus:
		executor := func() {
			for {
				err := conn.Subscribe(args)
				if err != nil {
					p.b.CausePanic(
						"Unable to subscribe (Azure)",
						err,
					)
				}
			}
		}
		go executor()
	}
}
