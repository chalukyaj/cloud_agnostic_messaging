# CloudAgnosticMessaging

#### _Use just this library to effectively use the same code to **Publish** and **Subscribe** accross any of the these cloud providers: AWS, GCP and Azure_

###### _TBC: Will be adding support for sending and receiving messages from Queue accross all clouds shortly along with some minor extension of the pubsub lib to support more options that the underlying sdks provide_

This project aims to help migration to single tenant solutions and allowing to write cloud agnostic code for Applications dependent on pubsub without having to resort to self managed solutions such as kafka. This also means that this repo can be forked and extended to allow pubsub with kafka along with other cloud provided pubsub solutions.
This also acts as an efficient wrapper around your favourite cloud sdk to provide simpler implementations of application consuming PubSub.

##### Features
- Same method signature and common interface to publish and subscribe to messages accross all clouds
- Automatic translation of message attributes to whatever data type and formatting _(for eg to rearrange simple `key:value` pair to `key:{Type,Value}` for aws, json encoding values whenever they aren't simple types, etc)_ the data to fit any cloud's sdk
- Automatic json encoding and decoding on publish and subscribe end respectively
- Runs all pub and sub methods internally as gocoroutines which allows async messaging
- Subscription Data sent back over a GoLang channel to allow message processing logic at client end rather than passing a callback method
- Ensures the published message and attribute data types match the received message and attribute data types which allows much less code for implementing PubSub


##### Examples
See this [Example Code](https://gitlab.com/chalukyaj/cloud_agnostic_messaging/-/blob/main/examples/PubSubAsync.go) which demonstrates asynchronous pubish and subscribe running concurrently along with various configuration arguments.

##### Snippets and Configurations
###### _Installation_
```bash
go get gitlab.com/chalukyaj/cloud_agnostic_messaging@v1.4.2
```
###### _Code Usage_
```golang
import (
...
"gitlab.com/chalukyaj/cloud_agnostic_messaging"		//Import
...
)

//Init
...
pubsub := new(cloud_agnostic_messaging.PubSub)
pubsub.Init(map[string]interface{}{
		"account_id":      AWS_ACCOUNT_ID,
		"region":          AWS_REGION,
		"project":         PROJECT,
		"gcp_project":     GCP_PROJECT,
		"namespace":       AZ_SB_NAMESPACE,
		"access_key":      AZ_SB_ACCESS_KEY,
		"access_key_name": AZ_SB_ACCESS_KEY_NAME,
		"entity_path":     AZURE_ENTITY_PATH,
	})
...

//Publish
...
err := pubsub.Publish(map[string]interface{}{
			"topic_name": TOPIC_NAME,
			"msg_body": MSG_BODY (type : anything json marshallable (only converted to json if not primitive types)),
			"msg_attr": MSG_ATTR (type : anything with json marshallable (only converted to json if not primitive types) individual attributes),,
		})
		if err != nil {
			panic([]interface{}{
				"Error Publishing Message",
				err,
			})
		}
...

//Subscribe
...
msgChannel := make(chan map[string]interface{}) //Create a channel to receive subscription messages in
go p.Subscribe(
	map[string]interface{}{
		"topic_name": TOPIC_NAME,
		"sub_name":   SUB_NAME,
		"queue_name": AWS_SQS_QName,
		"protocol":   AWS_SUB_PROTOCOL,
		"visibility": 30,
		"callback_args": CALLBACK_ARGS (type: map[string]interface{}),
		"attr": map[string]string{
			"RawMessageDelivery": "True|False",
		},
	},
	msgChannel, //Pass the channel to Subscribe method to allow it to send messages back on it
)
for {
    msgData <- msgChannel  //Read Message from the channel
    ...
    //process message data
    ...
}
...
```
##### _Methods Available and Configurations Possible_
Below is the list of all configuration values that can be sent to the Library.
Note that all the methods listed below takes `args` in `map[string]interface{}` datatype. The `key` column lists the key that needs to be sent in the `args` map.


Method | Key | Type | Required/Optional | Description 
------ | --- | ---- | ----------------- | -----------
PubSub.**Init**(args **_map[string]interface{}_**) | `project` | `string` | Required | Cloud on which the code is running. Possible values `aws\|gcp\|azure`
|| `account_id` | `string` | Required when **i)** The `project` is `aws` | 12 Digit AWS Account ID that will be used to auto construct the ARNs for resources
|| `region` | `string` | Required when **i)** The `project` is `aws` | AWS Region
|| `gcp_project` | `string` | Required when **i)** The `project` is `gcp` | GCP Project Name
|| `namespace` | `string` | Required when **i)** The `project` is `azure` | Azure ServiceBus Namespace
||  `access_key` | `string` | Required when **i)** The `project` is `azure` **ii)** the Topic/Subscription cannot be accessed without a key | Azure Access Key
||  `access_key_name` | `string` | Required when **i)** The `project` is `azure` **ii)** The Topic/Subscription cannot be accessed without a key | Azure Access Key Name
||  `entity_path` | `string` | Required when **i)** The `project` is `azure` **ii)** The Topic/Subscription cannot be accessed without a key **iii)** The access key is entity specific | Azure Access Key Entity Path
||
PubSub.**Publish**(args **_map[string]interface{}_**) error | `topic_name` | `string` | Required | Topic Name of the topic to publish message to
|| `msg_body` | `map[string]interface{}` | Required | Message Body is expected to be in `map[string]interface{}` datatype. The values against any key should be json serializable
|| `msg_attr` | `map[string]interface{}` | Required | Message Attributes is expected to be in `map[string]interface{}` datatype. The values against any key should be json serializable
||
PubSub.**Subscribe**(args **_map[string]interface{}_**, msgChannel **_chan map[string]interface{}_**) | `topic_name` | `string` | Required | Topic Name of the topic to which the messages were published
|| `sub_name` | `string` | Required when **i)** The `project` is `gcp\|azure` | Name of the Subscription
|| `queue_name` | `string` | Required when **i)** The `project` is `aws` | Name of the Subscription
|| `attr` | `map[string]string` | _(Optional)_ Only used when **i)** The `project` is `aws`. Defaults to `map[string]interface{}{"RawMessageDelivery": "True"}` | AWS SNS Subscribe Attributes
|| `protocol` | `string` | _(Optional)_ Only used when **i)** The `project` is `aws`. Defaults to `sqs` | AWS Subscription Protocol
|| `max_messages` | `int` | _(Optional)_ Only used when **i)** The `project` is `aws\|azure`. Defaults to `10` | Maximum Messages parameter for corresponding cloud's sdk
|| `wait_time` | `int` | _(Optional)_ Only used when **i)** The `project` is `aws`. Defaults to `20` | Wait Time Seconds Parameter in AWS SDK
|| `visibility` | `int` | _(Optional)_ Only used when **i)** The `project` is `aws`. Defaults to `120` | Visibility Timeout Parameter in AWS SDK

###### Received Message Structure
Messages are received from the GoLang channel variable that was passed to the `PubSub.Subscribe` method.
_For eg:_ `msgData := <- msgChannel`
The Message received is of the type map[string]interface{} with the following keys
Key | Type | Description
--- | ---- | -----------
`msg_id` | `string` | Message ID of the received message
`msg_body` | `map[string]interface{}` | Message Body of the received message
`msg_attr` | `map[string]interface{}` | Message Attributes of the received message
`arg1` | `interface{}` | Any `key:value` pair in `callback_args` that were sent to the `Subscribe` method will be sent here. For eg. If you sent `callback_args` as `map[string]interface{}{"arg1": "examplevalue"}` then the key `arg1` will be present in `msgData`

#### Changelog
- v1.4.2
	- Added proper support for handling `RawMessageDelivery` in AWS SNS subscriptions
- v1.3.2
	- Non code related changes
- v1.3.1
	- Proper fix released for azure messages not getting acked due to subscription connection being closed prematurely
- v1.2.1
	- Updated README.md to add/update documentation
- v1.1.1
	- Bug fix for azure message not getting Acked 
- v1.1.0
	- Initial Library
	- Support for AWS/GCP/Azure PubSub implementations
## License

This code is distributed under the GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 license, see the [LICENSE](https://gitlab.com/chalukyaj/cloud_agnostic_messaging/-/blob/main/LICENSE) file.
