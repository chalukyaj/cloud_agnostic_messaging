/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	"gitlab.com/chalukyaj/cloud_agnostic_messaging"
)

var p = new(cloud_agnostic_messaging.PubSub)

// CLOUD TYPE
var PROJECT = "<aws|gcp|azure>"

// TOPIC & SUB Names
var TOPIC_NAME = "<topic_name for the corresponding cloud>"
var SUB_NAME = "<subscription_name for the corresponding cloud>"

// TOPIC & Sub Extras (AWS)
var AWS_SQS_QName = "<aws sqs name if cloud is aws and if subscription protocol `sqs`>"
var AWS_SUB_PROTOCOL = "<aws subscription protocol. Defaults to `sqs` if not set which will make `AWS_SQS_QName` mandatory>"

// AWS Credentials
var AWS_ACCOUNT_ID = "<aws_account_id>"
var AWS_REGION = "<aws_region>"

// GCP Credentials (Along with Environment Variable `GOOGLE_APPLICATION_CREDENTIALS`)
var GCP_PROJECT = "<gcp_project_name>"

// Azure credentials if it is required
var AZ_SB_NAMESPACE = "<azure_service_bus_namespace>"             //Mandatory
var AZURE_ENTITY_PATH = "<azure_entity_path>"                     //Optional
var AZ_SB_ACCESS_KEY = "<azure_service_bus_access_key>"           //Optional
var AZ_SB_ACCESS_KEY_NAME = "<azure_service_bus_access_key_name>" //Optional

var RUN_FOR_SECONDS = 30
var PUBLISH_COUNT = 10

/*
Publish Messages with `uuid` of the run and other attributes
*/
func pub(done chan bool, uid string) {
	//Publish message `PUBLISH_COUNT` times
	for i := 0; i < PUBLISH_COUNT; i = i + 1 {
		err := p.Publish(map[string]interface{}{
			"topic_name": TOPIC_NAME,
			"msg_body": map[string]interface{}{
				"testbody":  fmt.Sprintf("Here is a test message %s %d %d %d", uid, i, i, i),
				"testother": "something random",
			},
			"msg_attr": map[string]interface{}{
				"test_time": time.Now(),
				"test_str":  "test attr string",
				"test_arr":  []int{1, 2, 3, 4},
				"test_map": map[string]interface{}{
					"mapattr": "map value",
					"key":     "value",
				},
			},
		})
		if err != nil {
			panic([]interface{}{
				"Error Publishing Message",
				err,
			})
		}
		fmt.Println("Sent Message ID : ", i)
		time.Sleep(time.Second) //Delay between sending consecutive messages
	}
	done <- true //Mark Publishing Method is done
}

func sub(done chan bool) {
	msgChannel := make(chan map[string]interface{}) //Create a channel to receive subscription messages in
	go p.Subscribe(
		map[string]interface{}{
			"topic_name": TOPIC_NAME,
			"sub_name":   SUB_NAME,
			"queue_name": AWS_SQS_QName,
			"protocol":   AWS_SUB_PROTOCOL,
			"visibility": 30,
			"callback_args": map[string]interface{}{ //Arguments which will be sent to the callback along with Message Data
				"testarg": "test",
				"arg_arr": []int{1, 2, 3},
			},
			"attr": map[string]string{ //Optional
				"RawMessageDelivery": "True|False",
			},
		},
		msgChannel, //Pass the channel to Subscribe method to allow it to send messages back on it
	)

	//Callback function to receive and process message on client's end
	executor := func(msgChan chan map[string]interface{}) {
		for {
			select { //Select to make sure channel is read properly in high concurrency
			case args := <-msgChan: //Read messages from channel
				fmt.Println("\n\n----------------------")
				fmt.Println("Recieved Message")
				fmt.Println("Message ID: ", args["msg_id"].(string))

				body := args["msg_body"].(map[string]interface{})
				fmt.Println("Message Body", body)

				fmt.Println("Message Attribute", args["msg_attr"])
				fmt.Println("Callback Args", args["testarg"])
				fmt.Println("Callback Args", args["arg_arr"])
				fmt.Println("----------------------\n\n")
			}
			time.Sleep(time.Second)
		}
	}
	//Process subscriptions parellely while moving forward to do other things
	go executor(msgChannel)

	time.Sleep(time.Duration(RUN_FOR_SECONDS) * time.Second)
	done <- true //Mark Subscription Method is done
}

func main() {
	/*
		Initialize PubSub object. Only provide values required for the cloud that the code is going to run in.
		For eg.
			Case AWS:
				`AWS_ACCOUNT_ID`, `AWS_REGION`, `TOPIC_NAME`, `AWS_SQS_QName`, `AWS_SUB_PROTOCOL` are required
			Case GCP:
				ENVIRONMENT VARIABLE `GOOGLE_APPLICATION_CREDENTIALS` required
				`GCP_PROJECT`, `TOPIC_NAME`, `SUB_NAME` are required
			Case Azure:
				`AZ_SB_NAMESPACE`, `AZ_SB_ACCESS_KEY`, `AZ_SB_ACCESS_KEY_NAME`, `AZURE_ENTITY_PATH`, `TOPIC_NAME`, `SUB_NAME` are required
	*/

	p.Init(map[string]interface{}{
		"account_id":      AWS_ACCOUNT_ID,
		"region":          AWS_REGION,
		"project":         PROJECT,
		"gcp_project":     GCP_PROJECT,
		"namespace":       AZ_SB_NAMESPACE,
		"access_key":      AZ_SB_ACCESS_KEY,
		"access_key_name": AZ_SB_ACCESS_KEY_NAME,
		"entity_path":     AZURE_ENTITY_PATH,
	})

	pubDone := make(chan bool)
	subDone := make(chan bool)
	go sub(subDone) //Run subscribe method before publish to prevent missing any message

	uid := uuid.New().String()
	fmt.Println("UUID for this run : ", uid) //Generate and attach an `uuid` to the messages published to allow distinguishing between messages from various run

	go pub(pubDone, uid)
	<-pubDone
	<-subDone
}
