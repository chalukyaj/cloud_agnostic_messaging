/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Azure

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"

	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/messaging/azservicebus"
	Base "gitlab.com/chalukyaj/cloud_agnostic_messaging/Base"
)

type SBus struct {
	b             Base.Base
	CONN          *azservicebus.Client
	Namespace     string
	AccessKeyName string
	AccessKey     string
	EntityPath    string
}

func (sbs *SBus) Init(args map[string]interface{}) {
	sbs.b.Init(args)
	sbs.Namespace = sbs.b.GetParam(args, "namespace").(string)
	sbs.AccessKeyName = sbs.b.GetParam(args, "access_key_name", "").(string)
	sbs.AccessKey = sbs.b.GetParam(args, "access_key", "").(string)
	sbs.EntityPath = sbs.b.GetParam(args, "entity_path", "").(string)

	var client *azservicebus.Client
	var err error

	if sbs.AccessKey != "" && sbs.AccessKeyName != "" {
		connStr := fmt.Sprintf(
			"Endpoint=sb://%s.servicebus.windows.net/;SharedAccessKeyName=%s;SharedAccessKey=%s",
			sbs.Namespace,
			sbs.AccessKeyName,
			sbs.AccessKey,
		)
		if sbs.EntityPath != "" {
			connStr = connStr + ";EntityPath=" + sbs.EntityPath
		}

		client, err = azservicebus.NewClientFromConnectionString(
			connStr,
			nil,
		)
	} else {
		cred, _ := azidentity.NewDefaultAzureCredential(nil)
		client, err = azservicebus.NewClient(
			sbs.Namespace,
			cred,
			nil,
		)
	}

	if err != nil {
		sbs.b.CausePanic(
			"Cannot create new Client (Azure)",
			err,
		)
	}

	sbs.CONN = client
}

func (sbs *SBus) Publish(args map[string]interface{}) error {
	sender, err := sbs.CONN.NewSender(sbs.b.GetParam(args, "topic_name").(string), nil)
	if err != nil {
		return err
	}
	defer sender.Close(context.Background())

	jsonBody, err := json.Marshal(sbs.b.GetParam(args, "msg_body").(map[string]interface{}))
	if err != nil {
		sbs.b.CausePanic(
			"Cannot json encode Msg Body (msg_body) (Azure)",
			err,
		)
	}

	msgAttr := make(map[string]interface{})
	sbs.b.AttrMapInterfaceToMapString(
		msgAttr,
		sbs.b.GetParam(args, "msg_attr", make(map[string]interface{})).(map[string]interface{}),
	)

	return sender.SendMessage(
		context.Background(),
		&azservicebus.Message{
			ApplicationProperties: msgAttr,
			Body:                  jsonBody,
		},
		nil,
	)
}

func (sbs *SBus) Subscribe(args map[string]interface{}) error {
	receiver, err := sbs.CONN.NewReceiverForSubscription(
		sbs.b.GetParam(args, "topic_name").(string),
		sbs.b.GetParam(args, "sub_name").(string),
		nil,
	)
	if err != nil {
		return err
	}
	defer receiver.Close(context.Background())

	messages, err := receiver.ReceiveMessages(
		context.Background(),
		sbs.b.GetParam(args, "max_messages", 10).(int),
		nil,
	)
	if err != nil {
		return err
	}

	callbackArgs := sbs.b.GetParam(
		args,
		"callback_args",
		make(map[string]interface{}),
	).(map[string]interface{})

	executor := func(msg *azservicebus.ReceivedMessage, args map[string]interface{}, cArgs map[string]interface{}, wg *sync.WaitGroup) {
		callbackArgs := make(map[string]interface{})
		for k, v := range cArgs {
			callbackArgs[k] = v
		}

		var data map[string]interface{}

		if err := json.Unmarshal([]byte(msg.Body), &data); err != nil {
			sbs.b.CausePanic(
				"Couldn't json decode Msg Body (Azure)",
				err,
			)
		}

		callbackArgs["msg_body"] = data
		callbackArgs["msg_id"] = msg.MessageID
		callbackArgs["msg_attr"] = sbs.b.AttrMapStringToMapInterface(
			msg.ApplicationProperties,
		)
		go args["callback"].(func(map[string]interface{}))(
			callbackArgs,
		)
		err := receiver.CompleteMessage(context.Background(), msg, nil)
		if err != nil {
			panic(err)
		}
		wg.Done()
	}

	var wg sync.WaitGroup

	for _, message := range messages {
		wg.Add(1)
		go executor(message, args, callbackArgs, &wg)
	}
	wg.Wait()
	return nil
}
