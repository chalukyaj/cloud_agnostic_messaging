/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package AWS

import (
	"context"
	"encoding/json"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	aws_sns "github.com/aws/aws-sdk-go-v2/service/sns"
	Base "gitlab.com/chalukyaj/cloud_agnostic_messaging/Base"
)

type SNS struct {
	b    Base.Base
	CONN *aws_sns.Client
}

func (sns *SNS) Init(args map[string]interface{}) {
	sns.b.Init(args)
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		sns.b.CausePanic(
			"Configuration error (SNS)",
			err,
		)
	}
	sns.CONN = aws_sns.NewFromConfig(cfg, func(opts *aws_sns.Options) {
		opts.Region = sns.b.REGION
	})
}

func (sns *SNS) Publish(args map[string]interface{}) error {
	msgBody, err := json.Marshal(
		sns.b.GetParam(args, "msg_body").(map[string]interface{}),
	)
	if err != nil {
		sns.b.CausePanic(
			"Error json encoding msg body (SNS)",
			err,
		)
	}
	_, err = sns.CONN.Publish(context.Background(), &aws_sns.PublishInput{
		Message: aws.String(string(msgBody)),
		MessageAttributes: sns.b.DictToAwsSnsAttr(
			sns.b.GetParam(
				args,
				"msg_attr",
				make(map[string]interface{}),
			).(map[string]interface{}),
		),
		TopicArn: aws.String(sns.b.GetParam(args, "topic_arn").(string)),
	})
	return err
}

func (sns *SNS) Subscribe(args map[string]interface{}) (*aws_sns.SubscribeOutput, error) {
	return sns.CONN.Subscribe(context.Background(), &aws_sns.SubscribeInput{
		Protocol:   aws.String(sns.b.GetParam(args, "protocol").(string)),
		TopicArn:   aws.String(sns.b.GetParam(args, "topic_arn", "").(string)),
		Endpoint:   aws.String(sns.b.GetParam(args, "endpoint", "").(string)),
		Attributes: sns.b.GetParam(args, "attr", make(map[string]string)).(map[string]string),
	})
}
