/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package AWS

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	aws_sqs "github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/aws/aws-sdk-go-v2/service/sqs/types"
	Base "gitlab.com/chalukyaj/cloud_agnostic_messaging/Base"
)

type SQS struct {
	b    Base.Base
	CONN *aws_sqs.Client
}

func (sqs *SQS) Init(args map[string]interface{}) {
	sqs.b.Init(args)
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		sqs.b.CausePanic(
			"Configuration error (SQS)",
			err,
		)
	}
	sqs.CONN = aws_sqs.NewFromConfig(cfg, func(opts *aws_sqs.Options) {
		opts.Region = sqs.b.REGION
	})
}

func (sqs *SQS) PublishMessage(args map[string]interface{}) error {
	queueURL := sqs.b.GetParam(args, "queue_name").(string)
	msgBody, err := json.Marshal(
		sqs.b.GetParam(args, "msg_body").(map[string]interface{}),
	)
	if err != nil {
		sqs.b.CausePanic(
			"Error json encoding msg body",
			err,
		)
	}
	msgAttr := sqs.b.DictToAwsSqsAttr(
		sqs.b.GetParam(
			args,
			"msg_attr",
			make(map[string]interface{}),
		).(map[string]interface{}),
	)

	_, err = sqs.CONN.SendMessage(context.Background(), &aws_sqs.SendMessageInput{
		MessageBody:       aws.String(string(msgBody)),
		QueueUrl:          aws.String(queueURL),
		MessageAttributes: msgAttr,
	})
	return err
}

func (sqs *SQS) ReceiveMessage(args map[string]interface{}) error {
	queueURL := sqs.b.GetParam(args, "queue_name").(string)
	maxMessages := int32(sqs.b.GetParam(args, "max_messages", 10).(int))
	waitTime := int32(sqs.b.GetParam(args, "wait_time", 20).(int))
	visibility := int32(sqs.b.GetParam(args, "visibility", 120).(int))
	msgAttr := sqs.b.GetParam(args, "attr", make(map[string]string)).(map[string]string)
	rawMessage, ok := msgAttr["RawMessageDelivery"]
	if !ok {
		rawMessage = "True"
	}

	msgRes, err := sqs.CONN.ReceiveMessage(context.Background(), &aws_sqs.ReceiveMessageInput{
		QueueUrl:            aws.String(queueURL),
		MaxNumberOfMessages: maxMessages,
		MessageAttributeNames: []string{
			string(types.QueueAttributeNameAll),
		},
		VisibilityTimeout: visibility,
		WaitTimeSeconds:   waitTime,
	})
	if err != nil {
		return err
	}
	if msgRes.Messages == nil {
		return nil
	}

	callbackArgs := sqs.b.GetParam(
		args,
		"callback_args",
		make(map[string]interface{}),
	).(map[string]interface{})

	executor := func(msg types.Message, args map[string]interface{}, cArgs map[string]interface{}) {
		callbackArgs := make(map[string]interface{})
		for k, v := range cArgs {
			callbackArgs[k] = v
		}

		var data map[string]interface{}

		if err := json.Unmarshal([]byte(*msg.Body), &data); err != nil {
			sqs.b.CausePanic(
				"Couldn't json decode Msg Body (SQS)",
				err,
			)
		}

		if rawMessage == "True" {
			callbackArgs["msg_body"] = data
			callbackArgs["msg_id"] = *msg.MessageId
			callbackArgs["msg_attr"] = sqs.b.AwsSqsAttrToDict(msg.MessageAttributes)
		} else {
			var body map[string]interface{}
			if err := json.Unmarshal([]byte(data["Message"].(string)), &body); err != nil {
				sqs.b.CausePanic(
					"Couldn't json decode Msg Body (SQS - SNS Msg)",
					err,
				)
			}
			callbackArgs["msg_body"] = body
			callbackArgs["msg_id"] = data["MessageId"]
			attr, ok := data["MessageAttributes"].(map[string]interface{})
			if !ok {
				attr = make(map[string]interface{})
			}
			callbackArgs["msg_attr"] = sqs.b.AwsSnsAttrToDict(attr)
		}

		go args["callback"].(func(map[string]interface{}))(
			callbackArgs,
		)
		go sqs.DeleteMessage(map[string]interface{}{
			"queue_name":     queueURL,
			"receipt_handle": *msg.ReceiptHandle,
		})
	}
	for _, val := range msgRes.Messages {
		go executor(val, args, callbackArgs)
	}
	return nil
}

func (sqs *SQS) DeleteMessage(args map[string]interface{}) {
	queueURL := sqs.b.GetParam(args, "queue_name").(string)
	receiptHandle := sqs.b.GetParam(args, "receipt_handle").(string)

	_, err := sqs.CONN.DeleteMessage(context.Background(), &aws_sqs.DeleteMessageInput{
		QueueUrl:      aws.String(queueURL),
		ReceiptHandle: aws.String(receiptHandle),
	})

	if err != nil {
		sqs.b.CausePanic(
			fmt.Sprintf(
				"Couldn't delete Message. Queue: %s\tReceiptHandle: %s",
				queueURL,
				receiptHandle,
			),
			err,
		)
	}
}
