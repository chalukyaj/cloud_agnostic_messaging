/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Base

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go-v2/aws"
	sns_types "github.com/aws/aws-sdk-go-v2/service/sns/types"
	sqs_types "github.com/aws/aws-sdk-go-v2/service/sqs/types"
)

type Base struct {
	REGION string
}

func (b *Base) Init(args map[string]interface{}) {
	b.REGION = b.GetParam(args, "region", "us-east-1").(string)
}

func (b *Base) GetAwsArn(resource string, region string, account_id string, identifier string) string {
	return fmt.Sprintf("arn:aws:%s:%s:%s:%s", resource, region, account_id, identifier)
}

func (b *Base) GetParam(args map[string]interface{}, key string, def ...interface{}) interface{} {
	val, ok := args[key]
	if !ok {
		if len(def) == 0 {
			b.CausePanic(
				fmt.Sprintf(
					"Missing Key in `args` : `%s`",
					key,
				),
				nil,
			)
		}
		return def[0]
	}
	return val
}

func (b *Base) CausePanic(desc string, err error) {
	if err == nil {
		panic(desc)
	}
	panic([]interface{}{
		desc,
		err,
	})
}

func (b *Base) DictToAwsSqsAttr(args map[string]interface{}) map[string]sqs_types.MessageAttributeValue {
	msgAttr := map[string]sqs_types.MessageAttributeValue{}

	for key, val := range args {
		switch v := val.(type) {
		case string:
			msgAttr[key] = sqs_types.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(v),
			}
			break
		case []byte:
			msgAttr[key] = sqs_types.MessageAttributeValue{
				DataType:    aws.String("Binary"),
				BinaryValue: v,
			}
			break
		case int:
			msgAttr[key] = sqs_types.MessageAttributeValue{
				DataType:    aws.String("Number"),
				StringValue: aws.String(strconv.Itoa(v)),
			}
			break
		default:
			val, err := json.Marshal(v)
			if err != nil {
				b.CausePanic(
					fmt.Sprintf(
						"Cannot json encode Message Attribute : %s",
						key,
					),
					nil,
				)
			}
			msgAttr[key+"-json"] = sqs_types.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(string(val)),
			}
		}
	}
	return msgAttr
}

func (b *Base) AwsSqsAttrToDict(args map[string]sqs_types.MessageAttributeValue) map[string]interface{} {
	msgAttr := make(map[string]interface{})

	for key, val := range args {
		switch *val.DataType {
		case "String":
			if strings.HasSuffix(key, "-json") {
				var data interface{}
				if err := json.Unmarshal([]byte(*val.StringValue), &data); err != nil {
					b.CausePanic(
						"Couldn't unmarshal message attribute",
						err,
					)
				}
				msgAttr[key[0:len(key)-5]] = data
				continue
			}
			msgAttr[key] = *val.StringValue
			break
		case "Binary":
			msgAttr[key] = val.BinaryValue
			break
		case "Number":
			strVal, err := strconv.Atoi(*val.StringValue)
			if err != nil {
				b.CausePanic(
					fmt.Sprintf(
						"Error occurred while converting string to number in Message Attribute field : %s",
						key,
					),
					nil,
				)
			}
			msgAttr[key] = strVal
			break
		}
	}
	return msgAttr
}

func (b *Base) DictToAwsSnsAttr(args map[string]interface{}) map[string]sns_types.MessageAttributeValue {
	msgAttr := map[string]sns_types.MessageAttributeValue{}

	for key, val := range args {
		switch v := val.(type) {
		case string:
			msgAttr[key] = sns_types.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(v),
			}
			break
		case []byte:
			msgAttr[key] = sns_types.MessageAttributeValue{
				DataType:    aws.String("Binary"),
				BinaryValue: v,
			}
			break
		case int:
			msgAttr[key] = sns_types.MessageAttributeValue{
				DataType:    aws.String("Number"),
				StringValue: aws.String(strconv.Itoa(v)),
			}
			break
		default:
			val, err := json.Marshal(v)
			if err != nil {
				b.CausePanic(
					fmt.Sprintf(
						"Cannot json encode Message Attribute : %s",
						key,
					),
					err,
				)
			}
			msgAttr[key+"-json"] = sns_types.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(string(val)),
			}
		}
	}
	return msgAttr
}

func (b *Base) AwsSnsAttrToDict(args map[string]interface{}) map[string]interface{} {
	msgAttr := make(map[string]interface{})

	for key, v := range args {
		val := v.(map[string]interface{})
		switch val["Type"] {
		case "String":
			if strings.HasSuffix(key, "-json") {
				var data interface{}
				if err := json.Unmarshal([]byte(val["Value"].(string)), &data); err != nil {
					b.CausePanic(
						"Couldn't unmarshal message attribute",
						err,
					)
				}
				msgAttr[key[0:len(key)-5]] = data
				continue
			}
			msgAttr[key] = val["Value"].(string)
			break
		case "Binary":
			msgAttr[key] = val["Value"].([]byte)
			break
		case "Number":
			strVal, err := strconv.Atoi(val["Value"].(string))
			if err != nil {
				b.CausePanic(
					fmt.Sprintf(
						"Error occurred in number field (Message Attribute) %s",
						key,
					),
					err,
				)
			}
			msgAttr[key] = strVal
			break
		}
	}
	return msgAttr
}

func (b *Base) AttrMapInterfaceToMapString(msgAttrs interface{}, args map[string]interface{}) {
	switch attrs := msgAttrs.(type) {
	case map[string]interface{}:
		for k, v := range args {
			switch val := v.(type) {
			case string:
				attrs[k] = val
				break
			case []byte:
				attrs[k] = string(val)
				break
			default:
				jsonVal, err := json.Marshal(val)
				if err != nil {
					b.CausePanic(
						"Cannot json encode message attribute",
						err,
					)
				}
				attrs[k+"-json"] = string(jsonVal)
			}
		}
	case map[string]string:
		for k, v := range args {
			switch val := v.(type) {
			case string:
				attrs[k] = val
				break
			case []byte:
				attrs[k] = string(val)
				break
			default:
				jsonVal, err := json.Marshal(val)
				if err != nil {
					b.CausePanic(
						"Cannot json encode message attribute",
						err,
					)
				}
				attrs[k+"-json"] = string(jsonVal)
			}
		}
	}
}

func (b *Base) AttrMapStringToMapInterface(args interface{}) map[string]interface{} {
	attrs := make(map[string]interface{})
	switch a := args.(type) {
	case map[string]string:
		for k, v := range a {
			if strings.HasSuffix(k, "-json") {
				k = k[0 : len(k)-5]
				var data interface{}
				if err := json.Unmarshal([]byte(v), &data); err != nil {
					b.CausePanic(
						"Couldn't unmarshal message attribute",
						err,
					)
				}
				attrs[k] = data
			} else {
				attrs[k] = v
			}
		}
	case map[string]interface{}:
		for k, v := range a {
			if strings.HasSuffix(k, "-json") {
				k = k[0 : len(k)-5]
				var data interface{}
				if err := json.Unmarshal([]byte(v.(string)), &data); err != nil {
					b.CausePanic(
						"Couldn't unmarshal message attribute",
						err,
					)
				}
				attrs[k] = data
			} else {
				attrs[k] = v.(string)
			}
		}
	}
	return attrs
}
