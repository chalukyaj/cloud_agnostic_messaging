/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GCP

import (
	"context"
	"encoding/json"

	"cloud.google.com/go/pubsub"
	Base "gitlab.com/chalukyaj/cloud_agnostic_messaging/Base"
)

type PubSub struct {
	b           Base.Base
	CONN        *pubsub.Client
	GCP_PROJECT string
}

func (gcp *PubSub) Init(args map[string]interface{}) {
	gcp.b.Init(args)
	gcp.GCP_PROJECT = gcp.b.GetParam(args, "gcp_project").(string)
	conn, err := pubsub.NewClient(context.Background(), gcp.GCP_PROJECT)
	if err != nil {
		gcp.b.CausePanic(
			"Couldn't create new PubSub (GCP) Client",
			err,
		)
	}
	gcp.CONN = conn
}

func (gcp *PubSub) Publish(args map[string]interface{}) error {
	jsonMsg, err := json.Marshal(
		gcp.b.GetParam(args, "msg_body"),
	)
	if err != nil {
		return err
	}
	msgAttr := make(map[string]string)
	gcp.b.AttrMapInterfaceToMapString(
		msgAttr,
		gcp.b.GetParam(args, "msg_attr", make(map[string]interface{})).(map[string]interface{}),
	)

	ctx := context.Background()
	topic := gcp.CONN.Topic(gcp.b.GetParam(args, "topic_name").(string))

	result := topic.Publish(ctx, &pubsub.Message{
		Data:       jsonMsg,
		Attributes: msgAttr,
	})
	// Block until the result is returned and a server-generated
	_, err = result.Get(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (gcp *PubSub) Subscribe(args map[string]interface{}) error {
	sub := gcp.CONN.Subscription(gcp.b.GetParam(args, "sub_name").(string))
	ctx := context.Background()

	callbackArgs := gcp.b.GetParam(
		args,
		"callback_args",
		make(map[string]interface{}),
	).(map[string]interface{})

	return sub.Receive(ctx, func(_ context.Context, msg *pubsub.Message) {
		cArgs := make(map[string]interface{})
		for k, v := range callbackArgs {
			cArgs[k] = v
		}

		var data map[string]interface{}

		if err := json.Unmarshal(msg.Data, &data); err != nil {
			gcp.b.CausePanic(
				"Couldn't json decode Msg Body",
				err,
			)
		}
		cArgs["msg_id"] = msg.ID
		cArgs["msg_body"] = data
		cArgs["msg_attr"] = gcp.b.AttrMapStringToMapInterface(msg.Attributes)

		go args["callback"].(func(map[string]interface{}))(
			cArgs,
		)
		go msg.Ack()
	})
}
